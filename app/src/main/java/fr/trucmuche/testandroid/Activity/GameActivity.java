package fr.trucmuche.testandroid.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;

import fr.trucmuche.testandroid.Model.Question;
import fr.trucmuche.testandroid.Model.QuestionBank;
import fr.trucmuche.testandroid.R;

public class GameActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "GameActivity " ;
    public static final String BUNDLE_EXTRA_SCORE = "BUNDLE_EXTRA_SCORE";

    // Déclarer les variables
    private TextView mQuestionTv;
    private Button mAnswer1Btn;
    private Button mAnswer2Btn;
    private Button mAnswer3Btn;
    private Button mAnswer4Btn;

    private QuestionBank mQuestionBank; // Banque de question
    private Question mCurrentQuestion;  // Question en cours
    private int mNumberOfQuestions; // Nombre de questions
    private int mScore; // Score

    // OnCreate().
    // C'est là où on initialise les variables, où on fait référence aux vues, où on ajoute des listeners
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate() Called");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game); // Récupère le fichier layout activity_game

        // Référencer les vues
        mQuestionTv = findViewById(R.id.activity_game_question_text);
        mAnswer1Btn = findViewById(R.id.activity_game_answer1_btn);
        mAnswer2Btn = findViewById(R.id.activity_game_answer2_btn);
        mAnswer3Btn = findViewById(R.id.activity_game_answer3_btn);
        mAnswer4Btn = findViewById(R.id.activity_game_answer4_btn);

        // Définir le nombre de questions qui vont être posées
        mNumberOfQuestions = 4;

        // Initialiser la banque de questions
        mQuestionBank = generateQuestions();

        // Utiliser la propriété TAG pour identifier les boutons et pouvoir s'en servir ensuite
        mAnswer1Btn.setTag(0);
        mAnswer2Btn.setTag(1);
        mAnswer3Btn.setTag(2);
        mAnswer4Btn.setTag(3);

        // Intercepter la réponse d'un utilisateur
        // La méthode OnClick est appelée chaque fois que l'utilisateur clique sur une réponse
        // Au lieu de réécrire 4 fois la même chose --> on fait une interface
        // Répétitif :
        /*
        mAnswer1Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Todo
            }
        });
        */
        // Non répétitif
        mAnswer1Btn.setOnClickListener(this);
        mAnswer2Btn.setOnClickListener(this);
        mAnswer3Btn.setOnClickListener(this);
        mAnswer4Btn.setOnClickListener(this);

        //  Set current question with good datas (Number of questions are not managed here)
        // Set la question courante (grâce au compteur on a le bon index)
        mCurrentQuestion = mQuestionBank.getQuestion();
        showQuestion(mCurrentQuestion); // Afficher la bonne question

    }

    private void showQuestion(final Question question) {
        Log.i(TAG, "showQuestion Called");
        // Set le texte de la question et le texte des 4 réponses
        mQuestionTv.setText(question.getQuestion());
        mAnswer1Btn.setText(question.getChoiceList().get(0)); // récupère la première réponse de la liste de réponses
        mAnswer2Btn.setText(question.getChoiceList().get(1));
        mAnswer3Btn.setText(question.getChoiceList().get(2));
        mAnswer4Btn.setText(question.getChoiceList().get(3));
    }

    // Générer des questions
    private QuestionBank generateQuestions() {
        Log.i(TAG, "Questions generated");
        Question question1 = new Question("What is the name of the current french president?",
                Arrays.asList("François Hollande", "Emmanuel Macron", "Jacques Chirac", "François Mitterand"), // Conversion d'un Array en liste
                1);

        Question question2 = new Question("How many countries are there in the European Union?",
                Arrays.asList("15", "24", "28", "32"),
                2);

        Question question3 = new Question("Who is the creator of the Android operating system?",
                Arrays.asList("Andy Rubin", "Steve Wozniak", "Jake Wharton", "Paul Smith"),
                0);

        Question question4 = new Question("When did the first man land on the moon?",
                Arrays.asList("1958", "1962", "1967", "1969"),
                3);

        Question question5 = new Question("What is the capital of Romania?",
                Arrays.asList("Bucarest", "Warsaw", "Budapest", "Berlin"),
                0);

        Question question6 = new Question("Who did the Mona Lisa paint?",
                Arrays.asList("Michelangelo", "Leonardo Da Vinci", "Raphael", "Carravagio"),
                1);

        Question question7 = new Question("In which city is the composer Frédéric Chopin buried?",
                Arrays.asList("Strasbourg", "Warsaw", "Paris", "Moscow"),
                2);

        Question question8 = new Question("What is the country top-level domain of Belgium?",
                Arrays.asList(".bg", ".bm", ".bl", ".be"),
                3);

        Question question9 = new Question("What is the house number of The Simpsons?",
                Arrays.asList("42", "101", "666", "742"),
                3);

        return new QuestionBank(Arrays.asList(question1,
                question2,
                question3,
                question4,
                question5,
                question6,
                question7,
                question8,
                question9));
    }

    // A chaque click sur une des réponses, il faut traiter ce qu'il se passe
    @Override
    public void onClick(View v) {
        Log.i(TAG, "onClick called");
        int responseIndex = (int) v.getTag(); // grâce au Tag qu'on a set sur chaque bouton, on peut identifier quel est l'index de la réponse

        // Si la réponse choisie est juste
        if (responseIndex ==  mCurrentQuestion.getAnswerIndex()) {
            Log.i(TAG, "Answer is true");
            mScore++; // On incrémente le score
            Toast.makeText(this, "GOOD ONE", Toast.LENGTH_LONG).show();
        } else { // Si la réponse est fausse
            Log.i(TAG, "Answer is false");
            Toast.makeText(this, "YOU ARE WRONG !", Toast.LENGTH_LONG).show();
        }

        // On décrémente le compteur du nombre de questions restantes à chaque fois qu'on a répondu à une question
        mNumberOfQuestions = mNumberOfQuestions -1;

        // S'il n'y a plus de question
        if(mNumberOfQuestions == 0) {
            Log.i(TAG, "End of the game");
            // END OF THE GAME

            // Version 1 : On affichait une alert indiquant que le jeu est fini.
            //AlertDialog.Builder builder = new AlertDialog.Builder(this);
              /*
            builder.setTitle("Well done!")
                    .setMessage("Your score is " + mScore)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish(); // Stop current Activity
                        }
                    })
                    .create()
                    .show();
            */

              // Version 2 : On récupère le score présent dans l'activité GameActivity et on le set dans setResult. On termine l'activité
            Intent intent = new Intent();
            intent.putExtra(BUNDLE_EXTRA_SCORE, mScore);
            setResult(RESULT_OK, intent);
            finish(); // Stop current Activity
            Log.i(TAG, " Finished Activity");

        } // S'il reste des questions
        else {
            Log.i(TAG, "There are more questions. Game continuing...");
            mCurrentQuestion = mQuestionBank.getQuestion(); // On récupère la bonne question (avec le bon index)
            showQuestion(mCurrentQuestion); // On affiche la question courrante
        }


    }
}
