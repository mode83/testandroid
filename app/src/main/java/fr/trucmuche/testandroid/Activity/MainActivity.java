package fr.trucmuche.testandroid.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import fr.trucmuche.testandroid.Model.User;
import fr.trucmuche.testandroid.R;

public class MainActivity extends AppCompatActivity {

    // Déclarer les variables
    private EditText mInputEt;
    private Button mPlayBtn;

    // Créer une nouvelle instance de User / Créer un nouvel utilisateur (qui pour l'instant n'a pas de prénom ni de nom ni rien
    private User mUser = new User();

    private static final int GAME_ACTIVITY_REQUEST_CODE = 69;
    private static final String TAG = "MainActivity : "; // Création d'une contante pour le debug

    // Récupérer une donnée de GameActivity, le score en l'occurence
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult() Called");
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "requestCode " + requestCode + "resultCode " + resultCode);

        if (GAME_ACTIVITY_REQUEST_CODE == requestCode && RESULT_OK == resultCode) {
            int score = data.getIntExtra(GameActivity.BUNDLE_EXTRA_SCORE, 0); // Get score data
            Toast.makeText(this, " Your score is : "+score, Toast.LENGTH_LONG).show(); // Display score data
        }
    }

    // OnCreate().
    // C'est là où on initialise les variables, où on fait référence aux vues, où on ajoute des listeners
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate() Called");
        super.onCreate(savedInstanceState); // Constructeur de la classe parente appelé. OnCreate prendra les paramètres de la classe parent.
        setContentView(R.layout.activity_main); // Récupère le fichier layout activity_main

        // Référencer les vues
        mInputEt = findViewById(R.id.activity_main_eTInput);
        mPlayBtn = findViewById(R.id.activity_main_button);

        // Par défaut, on désactive le bouton
        mPlayBtn.setEnabled(false);

        // Ajout d'un listener sur le bouton
        mInputEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.i(TAG, "BeforeTextChanged Called");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.i(TAG, "onTextChanged Called");
                mPlayBtn.setEnabled(s.toString().length() != 0); // Lorsque le texte change, on active le bouton
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.i(TAG, "afterTextChanged Called");
            }
        });

        // Ajout d'un listener sur le bouton
        mPlayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Button clicked");

                mUser.setFirstName(mInputEt.getText().toString()); // On SET le prénom de l'utilisateur à la donnée entrée dans l'EditText
                Log.i(TAG, "FirstName : "+ mUser.getFirstName());

                // Démarrer une nouvelle activité
                Intent gameActivity = new Intent(MainActivity.this, GameActivity.class ); // Premier paramètre : le contexte de notre Activité, Deuxième : Activité à charger
                startActivityForResult(gameActivity, GAME_ACTIVITY_REQUEST_CODE); // Démarrer = Intent de l'activité
            }
        });
    }
    @Override
    protected void onStart() {
        Log.i(TAG, " onStart called");
        super.onStart();
    }
    @Override
    protected void onResume() {
        Log.i(TAG, " onResume called");
        super.onResume();
    }
    @Override
    protected void onStop() {
        Log.i(TAG, " onStop called");
        super.onStop();
    }
    @Override
    protected void onDestroy() {
        Log.i(TAG, " onDestroy called");
        super.onDestroy();
    }
}
