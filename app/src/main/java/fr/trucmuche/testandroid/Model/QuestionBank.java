package fr.trucmuche.testandroid.Model;

import java.util.Collections;
import java.util.List;

public class QuestionBank {         // Classe qui permet de définir comment on génère une liste de questions
    private List<Question> questionList;    // Liste de question (Comme un tableau de questions mais extensible à volonté avec des méthodes très pratiques)
    private int nextQuestionIndex;  // Index de la question d'après

    // Constructeur qui prend en paramètre une liste de question
    public QuestionBank(List<Question> questionList) {
        nextQuestionIndex = 0;  // Initialisation de l'index à 0
        this.questionList = questionList;   // Initialisation de la liste de question
        Collections.shuffle(questionList); // Random l'ordre des questions de la List
}

    public Question getQuestion() {
        // Si l'index de la prochaine question est égal à la taille de la liste de questions
        if (nextQuestionIndex == questionList.size()) {
            nextQuestionIndex = 0; // Alors on réinitialise l'index (question d'après)
        }
        // A chaque appel de getQuestion(), on met un compteur pour renvoyer la bonne question
        Question myQuestion = questionList.get(nextQuestionIndex = nextQuestionIndex + 1 );
        return myQuestion;
    }
}