package fr.trucmuche.testandroid.Model;

public class User {
    private String firstName; // Prénom de l'utilisateur

    public String getFirstName() {  // Accesseur du Prénom
        return firstName;
    }

    public void setFirstName(String firstName) {       // Modifieur du Prénom
        this.firstName = firstName;
    }
}
