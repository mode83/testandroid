package fr.trucmuche.testandroid.Model;

import java.util.List;

public class Question {

    private String question;    // Question
    private List<String> choiceList;    // Réponses possibles
    private int answerIndex;    // Index de la réponse choisie

    // Constructeur de la classe Question.
    // Permet de définir tous les paramètres qu'une question peut avoir (
    // chaine de caractère correspondant à la question, Liste de chaines de caractères, index de la réponse choisie,...)
    public Question(String question, List<String> choiceList, int answerIndex) {
        this.question = question;
        this.choiceList = choiceList;
        this.answerIndex = answerIndex;
    }

    public String getQuestion() {       // Accesseur de Question
        return question;
    }
    public void setQuestion(String question) {      // Modifieur de Question
        this.question = question;
    }
    public List<String> getChoiceList() {       // Accesseur de la liste des réponses
        return choiceList;
    }
    public void setChoiceList(List<String> choiceList) {    // Modifieur de la liste des réponses
        this.choiceList = choiceList;
    }
    public int getAnswerIndex() {       // Accesseur de l'index de la réponse chosie
        return answerIndex;
    }
    public void setAnswerIndex(int answerIndex) {       // Modifieur de la réponse choisie
        this.answerIndex = answerIndex;
    }
}
